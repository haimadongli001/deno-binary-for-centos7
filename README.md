# deno-binary-for-centos7

#### 介绍
deno-binary-for-centos7


#### 编译教程

### 第一步，安装所需软件包：


```
yum upgrade -y && yum install -y git gcc && yum clean packages
```

### 第二步，安装rust


```
curl -sSf https://sh.rustup.rs | sh -s -- -y
```

### 第三步，拉取deno代码，并进入deno代码目录


```
git clone https://github.com/denoland/deno.git && cd deno
```

### 第四步，开始编译

```
 ~/.cargo/env && cargo build --release
```


### 若deno发布新版本，则更新代码再次编译

```
 git pull origin master

 ~/.cargo/env && cargo build --release
```


编译后即可在deno/target/release目录下找到deno二进制文件。


### 注意项：


1、国内访问 crates.io 速度比较慢，可以使用清华大学提供的镜像站点 [Rust crates.io 索引镜像使用帮助](https://mirrors.tuna.tsinghua.edu.cn/help/crates.io-index.git/)。


2、Github访问速度太慢的话，可以用 Gitee 极速下载 的地址代替：https://gitee.com/mirrors/deno ，需要注意码云上的版本是否已同步到最新版！


3、rusty_v8编译需要比较长时间，可以在[denoland/rusty_v8](https://github.com/denoland/rusty_v8/releases) 下载相应系统的lib文件，放到
deno/target/release/gn_out/obj 目录下，并将名称改为：librusty_v8.a 即可极大缩短编译时间。


4、1.8.2后，要更新gcc到4.9.4，避免rust组件编译失败。